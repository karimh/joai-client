$(function() {
  var old = {
    date: null,
    collection: null,
    group: null,
    first_row: null,
  };
  var sum = 0;

  var addGroupRow = function(group, sum, is_danger) {
    if (group.date === null)
      return;
    var tr = $("<tr>");
    tr.addClass("group");
    tr.append($("<td>").text(group.date));
    tr.append($("<td>"));
    tr.append($("<td>").text(group.collection));
    tr.append($("<td>").text(sum));
    tr.append($("<td>"),$("<td>"),$("<td>"),$("<td>"),$("<td>"),$("<td>"));
    if (is_danger)
      tr.addClass("danger");
    tr.insertBefore(group.first_row);

    var group_selector = "table tr." + group.group;
    tr.click(function(){
      $(group_selector).toggle();
    });
  };


  $("thead tr th").eq(0).text("Zeit");
  $("thead tr").prepend("<th>Tag</th>");
  $("tbody tr").each(function(idx, tr) {
    $(tr).addClass("log");
    var tds = $("td", tr);
    var date_td = $(tds).eq(0);
    var day = date_td.text().substring(0, 10);
    var time = date_td.text().substring(11);
    var message = $(tds).eq(7).text();
    if (message.match(/noRecordsMatch/))
      $(tr).removeClass("danger").addClass("warning");
    date_td.text(time);
    day_td = $("<td>").text(day);
    day_td.prependTo(tr);

  });


  $("table").tablesorter({
    theme: "bootstrap",
    widgets: ["uitheme", "stickyHeaders"],
    headerTemplate: '{content} {icon}',
    sortList: [
      [0, 1],
      [2, 0],
      [1, 1]
    ],
    headers : {
      0 : { sorter: false },
      1 : { sorter: false },
      2 : { sorter: false },
      3 : { sorter: false },
      4 : { sorter: false },
      5 : { sorter: false },
      6 : { sorter: false },
      7 : { sorter: false },
      8 : { sorter: false },
      9 : { sorter: false },
      10 : { sorter: false },
    },
  });
  $("table").removeClass("table-striped");

  is_danger = false;
  $("tbody tr").each(function(idx, tr) {
    var tds = $("td", tr);
    var date = $(tds).eq(0).text();
    var collection = $(tds).eq(2).text();
    var group = "group-" + date + "-" + collection;
    $(tr).addClass(group);

    if (old.date != date || old.collection != collection) { // neue Gruppe
      addGroupRow(old, sum, is_danger);
      old.first_row = $(tr);
      sum = 0;
      is_danger = false;
    }
    if ($(tr).hasClass("danger")) {
      is_danger = true;
    }
    count = parseInt($(tds).eq(3).text());
    if (!isNaN(count))
      sum = sum + count;

    old.date = date;
    old.collection = collection;
    old.group = group;
  });

  addGroupRow(old, sum, is_danger);

  $("tr.log").hide();

  console.log("done");
});
