package de.tib.hannover.joai.connector;

import de.tib.hannover.joai.client.JOAI;
import de.tib.hannover.joai.entity.impl.OaiPmhIpml;
import de.tib.hannover.joai.exeption.ServerExeption;
import de.tib.hannover.joai.util.DeleteEmptyFolder;
import de.tib.hannover.joai.util.JOAIUtils;
import de.tib.hannover.joai.util.MetadataValidator;
import de.tib.hannover.joai.util.OaiPmhConstants;
import de.tib.hannover.joai.util.Writer;
import de.tib.hannover.joai.validator.Validator;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;
import javax.net.ssl.SSLException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.DefaultServiceUnavailableRetryStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpConnector extends Validator {

  private static Logger LOG = LoggerFactory.getLogger(HttpConnector.class);

  private static final String HTTP_STATUS = "HTTP-Status: ";
  private static final String OAI_MESSAGE = "OAI-Abfrage:		";
  private static boolean fileError = false;
  private static final String ERROR = "/error/";
  private static final String ERROR_DESTINATION = "/error";
  private static final String METADATA = "/metadata";
  private static final String DOC_COUNT = "Dokumentenanzahl = ";
  private static final String NOTE = "Achtung: Spezialbehandlung!....";
  private static final String FATAL_TRANSPORT = " Fatal transport error: ";
  private static final String INVALID_DOCUMENT = "Achtung: Spezialbehandlung! Ein Datei ist ungültig wurde gelöscht!....";
  private static final String TOKEN = "/configAPIToken.properties";
  private static final String AUTHORIZATION = "Authorization";
  private static final int RETRY_DELAY_IN_SECONDS = 60;
  private static final int MAX_RETRY = 5;

  private OaiPmhIpml oaiPmh = null;
  private DeleteEmptyFolder empt = null;
  public static int index = 1;

  private static ArrayList<String> parameter = null;

  private boolean visible = false;
  private String resumptionToken = "";

  private String line = "\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";

  private SimpleDateFormat formatter = null;

  private Date currentTime = null;

  private static boolean requestValue = true;

  private static boolean urlExeption = false;

  private ArrayList<String> originalData = null;

  private String documentLocation = null;

  private static Properties properties = null;

  public static boolean dataContentInfo;

  private static String apiTokenValue;

  HttpGet httpRequest = null;

  HttpResponse response = null;

  CloseableHttpClient client = null;

  public boolean getComunication(String serverAddress, String from, String until,
      String metadataPrefix,
      String listCollection, String set) throws InterruptedException {

    final String setValue = (set == null) ? NOSET : set;   

    originalData = new ArrayList<String>();

    formatter = new SimpleDateFormat("HH:mm:ss");
    currentTime = new Date();

    String createDocument = "\n\n" + "Dokument " + getIndex() + "  Startet: " + formatter
        .format(getCurrentTime())
        + "  ";

    // Timeout in 15 minuten!
    int timeout = 900;

    final RequestConfig requestConfig = RequestConfig.custom()
        .setConnectTimeout(timeout * 1000)
        .setConnectionRequestTimeout(timeout * 1000)
        .setSocketTimeout(timeout * 1000)
        .build();

    client = HttpClientBuilder.create()
        .setDefaultRequestConfig(requestConfig)
        .setRetryHandler(new DefaultHttpRequestRetryHandler(MAX_RETRY, true, Arrays.asList(
            InterruptedIOException.class,
            UnknownHostException.class,
            //ConnectException.class,
            SSLException.class)) {

          @Override
          public boolean retryRequest(IOException exception, int executionCount,
              HttpContext context) {
            boolean retry = super.retryRequest(exception, executionCount, context);

            try {
              if (retry) {
                LOG.warn(LOG_PATTERN, getSupplierName(), setValue, "-",
                    "; " + "Erneurte Versuchsnummer: " + executionCount + " " + exception);

                Thread.sleep(RETRY_DELAY_IN_SECONDS * 1000);
              }
            } catch (InterruptedException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }

            return retry;
          }

        })
        .setServiceUnavailableRetryStrategy(new DefaultServiceUnavailableRetryStrategy() {

          @Override
          public boolean retryRequest(final HttpResponse response, final int executionCount,
              final HttpContext context) {

            int statusCode = response.getStatusLine().getStatusCode();

            boolean retry = executionCount <= MAX_RETRY && statusCode >= 500 && statusCode < 600;

            try {
              if (retry) {
                LOG.warn(LOG_PATTERN, getSupplierName(), setValue, statusCode,
                    statusCode + "; " + "Erneurte Versuchsnummer: " + executionCount);

                Thread.sleep(RETRY_DELAY_IN_SECONDS * 1000);
              }
            } catch (InterruptedException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }
            return retry;
          }

        }).build();

    if (!isVisible()) {

     try {
        
      String encodedSetValue = URLEncoder.encode(setValue, "UTF-8");

      //Leerezeichen wird auch codiert. .replace("+", "%20")
      String request = oaiPmh.getListRecords(serverAddress, from, until, metadataPrefix, encodedSetValue
          .replace("+", "%20"));

      System.out.println(OAI_MESSAGE + request + "\n" + line + createDocument + request);

      // verzögerung, damit der Server des Anbieters nicht belastet wird.
      Thread.sleep((long) JOAIUtils.getTime());

      /**
       * Content-Type: text/html;charset=UTF-8
       */

      httpRequest = new HttpGet(request);

      /**
       * ”Token” HTTP header with private access token! It must be configured in properties file.
       */
      apiTokenValue = getAPIToken();

      if (apiTokenValue != null) {
        httpRequest.addHeader(AUTHORIZATION, apiTokenValue);
        LOG.debug(LOG_PATTERN, getSupplierName(), setValue, "-", "API-Token wurde gesetzt!");
      }

      LOG.debug(LOG_PATTERN, getSupplierName(), setValue, "-", "[" + getIndex() + "] " + request);

      infoMessage(getIndex(), serverAddress, from, until, metadataPrefix, set);

      
      } catch (UnsupportedEncodingException e) {
        LOG.error("Issue while encoding" + e.getMessage());
        System.exit(3);
      }
      
    } else {

      // Es gibt Ausnahmefälle, dass der OAI-Server des Anbieters eine
      // Fehlermeldung
      // mit dem (Service Unavailable)-Status zurueck liefert.
      // Es liegt daran , dass Möglicherweise der Server
      // ueberlastet ist. Um das Problem zu beheben,
      // setzen wir in "configProperties" Datei den Zeit auf (20000
      // Millisekunde oder noch hoeher Wert) ein.
      // Verzeugerungen zwischen mehrere resumtiontoken Abfragen.
      Thread.sleep((long) JOAIUtils.getTime());

      String recordCollection = oaiPmh.getListRecordCollection(listCollection);

      System.out.println(createDocument + recordCollection);

      LOG.debug(LOG_PATTERN, getSupplierName(), setValue, "-",
          "[" + getIndex() + "] " + recordCollection);

      infoMessage(getIndex(), serverAddress, from, until, metadataPrefix, set);

      httpRequest = new HttpGet(recordCollection);

      if (apiTokenValue != null) {
        httpRequest.addHeader(AUTHORIZATION, apiTokenValue);
      }

    }

    try {

      response = client.execute(httpRequest);

    } catch (IOException e) {

      System.err.println("[" + getSupplierName() + "] " + "[" + setValue + "]" + FATAL_TRANSPORT
          + e.getMessage());
      LOG.error(LOG_PATTERN, getSupplierName(), setValue, "FATAL_TRANSPORT", e.getMessage());

      new Writer().statistics(HttpConnector.getInfoMessage(), true, FATAL_TRANSPORT + e.getMessage()
          + "");

    }

    int status = response.getStatusLine().getStatusCode();

    return getMessage(status, serverAddress, metadataPrefix, set, from, until);
  }

  private String getAPIToken() {
    properties = new Properties();
    String token = null;
    try {
      properties.load(new FileInputStream(
          new File(configDirectories() + TOKEN)));
      return token = (String) properties.get(getSupplierName());

    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return token;
  }

  public boolean getMessage(int httpStatusValue, String serverAddress, String metadataPrefix,
      String set,
      String from, String until) {

    String tokenValue = null;

    Writer writer = new Writer();

    BufferedReader bufferredReader = null;

    String setValue = (set == null) ? NOSET : set;

    try {

      if (httpStatusValue == HttpStatus.SC_NOT_IMPLEMENTED) {

        logError(httpStatusValue, writer, setValue, httpStatusValue, ServerExeption.NOT_IMPLEMENTED,
            6);

      } else if (httpStatusValue == HttpStatus.SC_SERVICE_UNAVAILABLE) {

        logError(httpStatusValue, writer, setValue, httpStatusValue,
            ServerExeption.SERVICE_UNAVAILABLE, 7);

      } else if (httpStatusValue == HttpStatus.SC_MOVED_TEMPORARILY) {

        logError(httpStatusValue, writer, setValue, httpStatusValue,
            ServerExeption.MOVED_TEMPORARILY, 8);

      } else if (httpStatusValue == HttpStatus.SC_FORBIDDEN) {

        logError(httpStatusValue, writer, setValue, httpStatusValue, ServerExeption.FORBIDDEN, 9);

      } else if (httpStatusValue == HttpStatus.SC_INTERNAL_SERVER_ERROR) {

        logError(httpStatusValue, writer, setValue, httpStatusValue,
            ServerExeption.INTERNAL_SERVER_ERROR,
            10);

      } else if (httpStatusValue == HttpStatus.SC_MOVED_PERMANENTLY) {

        logError(httpStatusValue, writer, setValue, httpStatusValue,
            ServerExeption.MOVED_PERMANENTLY, 11);

      } else if (httpStatusValue == HttpStatus.SC_GONE) {

        logError(httpStatusValue, writer, setValue, httpStatusValue, ServerExeption.GONE, 12);

      } else if (httpStatusValue == 429) {

        logError(httpStatusValue, writer, setValue, httpStatusValue,
            ServerExeption.TOO_MANY_REQUESTS, 13);

      } else if (httpStatusValue == HttpStatus.SC_NOT_FOUND) {

        logError(httpStatusValue, writer, setValue, httpStatusValue, ServerExeption.NOT_FOUND, 14);

      } else if (httpStatusValue == HttpStatus.SC_GATEWAY_TIMEOUT) {

        logError(httpStatusValue, writer, setValue, httpStatusValue, ServerExeption.GATEWAY_TIMEOUT,
            15);

      } else {

        BufferedReader rd = new BufferedReader(
            new InputStreamReader(response.getEntity().getContent()));

        String line = "";
        while ((line = rd.readLine()) != null) {
          getOriginalData().add(line);
        }

        setDocumentLocation(
            OaiPmhConstants.getMetadataDirectory() + OaiPmhConstants.getMetadatFileName()
                + StringUtils.replace(formatter.format(getCurrentTime()), ":", "") + "_"
                + getIndex() + ".xml");

        writeMetadataInFileSystem(getDocumentLocation());

        boolean errorMessage = MetadataValidator
            .getError(getDocumentLocation(), getSupplierName(), setValue);

        // Spezial-Behandlung
        if (MetadataValidator.isFlage()) {
          setResumptionToken("");
          setVisible(true);

          remove(false);

          LOG.debug(LOG_PATTERN, getSupplierName(), setValue, "-", INVALID_DOCUMENT);
          System.out.println("\n" + INVALID_DOCUMENT + "[" + getSupplierName() + "]");

          setCurrentTime(null);
          MetadataValidator.setFlage(false);
          infoMessage(getIndex() - 1, serverAddress, from, until, metadataPrefix, set);
          return false;

        } else if (!errorMessage) {

          setDataContentInfo(errorMessage);

          /**
           * Falles eine Error-Meldung vom OAI-Server geliefert wird, wird ein Error-Verzeichnis
           * erstellt und das Error-Dokument in dieses Verzeichnis verschoben.
           */

          System.err.println(
              "\nError-Verzeichnis: " + StringUtils.replace(OaiPmhConstants.getMetadataDirectory(),
                  getSupplierName() + METADATA, getSupplierName() + ERROR_DESTINATION));
          moveFile(false);

        } else if (errorMessage) {
          //          System.out.println(HTTP_STATUS + httpStatusValue + " OK");
          setDataContentInfo(errorMessage);

          setCurrentTime(null);

          tokenValue = MetadataValidator.getResumptionTokenValue(getIndex(), getDocumentLocation(),
              JOAIUtils.getConfigPattern());

          /**
           * Neu resumtionToken wird ermitelt.
           */

          if (!StringUtils.equals(tokenValue, getResumptionToken())) {
            setResumptionToken(tokenValue);

          } else if (StringUtils.equals(tokenValue, getResumptionToken()) && ((tokenValue != null
              && StringUtils.isNotEmpty(tokenValue))
              && (getResumptionToken() != null && StringUtils.isNotEmpty(getResumptionToken())))) {
            // Wenn letzte Dokument kein resumsionToken-Elemnet
            // beinhaltet z.B. (Base-Verlag),
            // wird das Programm nicht sofort beendet, sondern
            // bearbeitet
            // noch weitere Sets, wenn noch weitere Sets gibt.

            LOG.debug(LOG_PATTERN, getSupplierName(), setValue, "-", NOTE);
            System.out.println("\n" + NOTE + "[" + getSupplierName() + "]");
            setCurrentTime(null);
            setResumptionToken("");

          }

        }

        setVisible(true);
      }

    } catch (SocketException e) {
      /**
       * Serverkommunikation ist fehlgeschlagen! Es wird drei mal Versucht Server zu kommunizieren.
       * Jedes Kommunikationsaufbau dauert 30 Minuten.
       */

    } catch (Exception exeption) {

      LOG.error(LOG_PATTERN, getSupplierName(), setValue, exeption.getMessage(),
          httpStatusValue + "; " + null);

      System.err.println(exeption.getMessage());

    } finally {

      if (bufferredReader != null) {
        try {
          bufferredReader.close();
        } catch (Exception exeption) {
          System.err.println("finally Exeption :" + exeption);
        }
      }
    }

    index++;
    return true;
  }

  private void logError(int statusCode, Writer writer, String setValue, int httpStatusValue,
      String serverExeption,
      int exitNum) {

    LOG.error(LOG_PATTERN, getSupplierName(), setValue, httpStatusValue,
        statusCode + "; " + serverExeption);

    System.err.println("HTTP-Status: " + statusCode);

    writer.statistics(HttpConnector.getInfoMessage(), true, HTTP_STATUS + statusCode);

    setDataContentInfo(false);

    moveFile(true);

    System.exit(exitNum);

  }

  private void writeMetadataInFileSystem(String path) {
    PrintWriter printWriter = null;
    try {
      printWriter = new PrintWriter(new OutputStreamWriter(new FileOutputStream(path), "UTF-8"));

      for (int i = 0; i < getOriginalData().size(); i++) {
        printWriter.println(getOriginalData().get(i));
      }
      /**
       * wert =true , damit nur einmal request-Element bewertet wird. nicht alle requestes
       */

    } catch (IOException e) {
      LOG.error("Error creating file! " + e);
      System.exit(2);
    } finally {

      if (printWriter != null) {
        printWriter.flush();
        printWriter.close();
        getOriginalData().clear();
      }
    }

  }

  private void remove(boolean flage) {

    File supplierDirectory = null;
    File errorFile = null;

    supplierDirectory = new File(JOAI.getWorkingDirectory().trim() + getSupplierName().trim());

    errorFile = new File(JOAI.getMakeSupplierDir() + "/" + OaiPmhConstants.getMetadatFileName()
        + StringUtils.replace(formatter.format(getCurrentTime()), ":", "") + "_" + getIndex()
        + ".xml");

    // Error-File wird in den error-Verzeichnis kopiert und in das Metadaten
    // -Verzeichnis geloecht.
    errorFile.delete();

    // Die Leere-Verzeichnise werden aufgereumt.
    empt = new DeleteEmptyFolder(supplierDirectory);
    empt.deleteEmptyFolder();

    setCurrentTime(null);
  }

  private void moveFile(boolean flage) {

    File supplierDirectory = new File(JOAI.getWorkingDirectory().trim() + getSupplierName().trim());

    String valueAfterSupplierName = StringUtils
        .substringAfterLast(JOAI.getMakeSupplierDir().getAbsolutePath(),
            getSupplierName() + "/");

    File errorDirectory = new File(supplierDirectory.getAbsolutePath() + new File(ERROR));

    File errorFile = new File(JOAI.getMakeSupplierDir() + "/" + OaiPmhConstants.getMetadatFileName()
        + StringUtils.replace(formatter.format(getCurrentTime()), ":", "") + "_" + getIndex()
        + ".xml");

    String destDir = errorDirectory + "/"
        + StringUtils.replace(valueAfterSupplierName, "metadata/", "");

    try {

      FileUtils.copyFileToDirectory(errorFile, new File(destDir), true);

    } catch (IOException e) {

      if (!flage) {
        LOG.error("Datei [ " + errorFile + " ] " + "kann nicht kopiert werden.\n" + e.getMessage());
      }
    }

    // Error-File wird in den error-Verzeichnis kopiert und in das Metadaten
    // -Verzeichnis geloecht.
    errorFile.delete();

    // Die Leere-Verzeichnise werden aufgereumt.
    empt = new DeleteEmptyFolder(supplierDirectory);
    empt.deleteEmptyFolder();

    setCurrentTime(null);
  }

  private static String infoMessage(int i, String serverAddresse, String from, String until,
      String metadataPrefix,
      String set) {

    parameter = new ArrayList<String>();
    String ii = String.valueOf(i);

    parameter.add(getSupplierName());
    parameter.add(DOC_COUNT + ii + "");
    parameter.add(from);
    parameter.add(until);
    parameter.add(set);
    parameter.add(metadataPrefix);

    return null;
  }

  public static String[] getInfoMessage() {
    String[] result = new String[6];

    final DateFormat dfIso = new SimpleDateFormat("yyyy-MM-dd");

    Date currentTime = new Date();

    Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
    calendar.setTime(currentTime);

    result[0] = "Lieferant = " + parameter.get(0);

    if (isDataContentInfo()) {
      result[1] = parameter.get(1);
    }
    if (!StringUtils.isEmpty(parameter.get(2))) {
      result[2] = "From = " + parameter.get(2);
    }
    if (!StringUtils.isEmpty(parameter.get(3))) {
      result[3] = "Until = " + parameter.get(3);
    } else {
      result[3] = "Until = " + dfIso.format(calendar.getTime());
    }

    if (!StringUtils.isEmpty(parameter.get(4))) {
      result[4] = "Set = " + parameter.get(4);
    }
    if (!StringUtils.isEmpty(parameter.get(5))) {
      result[5] = "Metadatenformat = " + parameter.get(5) + "\n";
    }
    return result;
  }

  public static boolean isDataContentInfo() {
    return dataContentInfo;
  }

  public static void setDataContentInfo(boolean dataContentInfo) {
    HttpConnector.dataContentInfo = dataContentInfo;
  }

  public String getDocumentLocation() {
    return documentLocation;
  }

  public void setDocumentLocation(String documentLocation) {
    this.documentLocation = documentLocation;
  }

  public static boolean isFileError() {
    return fileError;
  }

  public static void setFileError(boolean fileError) {
    HttpConnector.fileError = fileError;
  }

  public static boolean isUrlExeption() {
    return urlExeption;
  }

  public static void setUrlExeption(boolean urlExeption) {
    HttpConnector.urlExeption = urlExeption;
  }

  public static boolean isRequestValue() {
    return requestValue;
  }

  public static void setRequestValue(boolean requestValue) {
    HttpConnector.requestValue = requestValue;
  }

  public String getResumptionToken() {
    return resumptionToken;
  }

  public void setResumptionToken(String resump) {
    this.resumptionToken = resump;
  }

  public void httpConnector(final OaiPmhIpml oaipmh) {
    this.oaiPmh = oaipmh;
  }

  public ArrayList<String> getOriginalData() {
    return originalData;
  }

  public void setOriginalData(ArrayList<String> originalData) {
    this.originalData = originalData;
  }

  public boolean isVisible() {
    return visible;
  }

  public void setVisible(boolean wert) {
    this.visible = wert;
  }

  public static int getIndex() {
    return index;
  }

  public static void setIndex(int index) {
    HttpConnector.index = index;
  }

  public Date getCurrentTime() {
    return currentTime;
  }

  public void setCurrentTime(Date currentTime) {
    this.currentTime = currentTime;
  }

}
