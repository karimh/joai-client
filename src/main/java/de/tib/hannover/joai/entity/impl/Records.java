package de.tib.hannover.joai.entity.impl;

import de.tib.hannover.joai.connector.HttpConnector;
import de.tib.hannover.joai.exeption.JoaiExeption;
import de.tib.hannover.joai.service.HarvestService;
import de.tib.hannover.joai.util.MetadataValidator;
import de.tib.hannover.joai.util.SupplierData;
import de.tib.hannover.joai.util.Writer;
import de.tib.hannover.joai.validator.Validator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Records extends Validator {

  public static Logger LOG = LoggerFactory.getLogger(Records.class);

  protected boolean finished = false;

  private static final String NORECORD = "noRecordsMatch";
  private static final String NODATA = "\nKeine Datensammlung! -------\n";
  private static final String CODE = "code=";

  private String getRecords(String url, String from, String until, String metadataPrefix,
      String set) {

    String setValue = (set == null) ? NOSET : set;

    HarvestService harvestService = new Harvest();

    int i = 0;

    Writer writer = new Writer();

    do {

      /**
       * requestValue =true. fuer die naeste OAI-Abfrage
       */
      HttpConnector.setRequestValue(true);

      finished = harvestService.harvesting(url, from, until, metadataPrefix, set, set);

      i++;

    } while (finished = true && i < 1);

    if (HttpConnector.isDataContentInfo()) {

      System.out.println("\nDie Dateien wurden erfolgreich heruntergeladen. --------\n");

      String result = "";
      //TODO: DRY
      for (String string : HttpConnector.getInfoMessage()) {
        if (!StringUtils.isEmpty(string)) {
          System.out.println(string);
          result += string.trim() + ", ";
        }
      }

      LOG.info(LOG_PATTERN, getSupplierName(), setValue, "BILANZ",
          StringUtils.removeEnd(result.trim(), ","));

      writer.statistics(HttpConnector.getInfoMessage(), false, "");

    } else if (!HttpConnector.isDataContentInfo()) {

      System.out.println(NODATA);

      String errorCode = "";

      if (!StringUtils.contains(MetadataValidator.getMessage(), JoaiExeption.CONTENTINVALID)) {

        errorCode = StringUtils.substringBetween(MetadataValidator.getMessage(), CODE, ">")
            .replace("\"", "");

      }

      if (StringUtils.contains(errorCode, NORECORD)) {

        String norecord = "";

        LOG.info(LOG_PATTERN, getSupplierName(), setValue, errorCode, "");

        //TODO: DRY
        for (String string : HttpConnector.getInfoMessage()) {
          if (!StringUtils.isEmpty(string)) {
            System.out.println(string);
            norecord += string.trim() + ", ";
          }
        }
        LOG.debug(LOG_PATTERN, getSupplierName(), setValue, errorCode,
            StringUtils.removeEnd(norecord.trim(), ","));

      } else {

        if (!StringUtils.isEmpty(errorCode)) {
          LOG.error(LOG_PATTERN, getSupplierName(), setValue, errorCode, "");
        }

        String error = "";

        //TODO: DRY
        for (String string : HttpConnector.getInfoMessage()) {
          if (!StringUtils.isEmpty(string)) {
            System.out.println(string);
            error += string.trim() + ", ";
          }
        }
        LOG.debug(LOG_PATTERN, getSupplierName(), setValue, errorCode,
            StringUtils.removeEnd(error.trim(), ","));
      }

      writer.statistics(HttpConnector.getInfoMessage(), true, "");
    }

    return null;
  }

  public void supplierDetails(SupplierData supplierData) {

    getRecords(supplierData.getUrl(), supplierData.getFrom(), supplierData.getUntil(),
        supplierData.getMetadataPrefix(), supplierData.getSet());
  }
}
