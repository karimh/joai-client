package de.tib.hannover.joai.entity.impl;

import de.tib.hannover.joai.connector.HttpConnector;
import de.tib.hannover.joai.entity.HarvestDomain;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OaiPmhIpml extends HttpConnector implements HarvestDomain {

  private static Logger LOG = LoggerFactory.getLogger(OaiPmhIpml.class);

  private final String LIST_SETS = "=ListSets";
  private final String LIST_FORMAT = "=ListMetadataFormats";
  private final String UNTIL = "&until=";
  private final String FROM = "&from=";
  private final String SET = "&set=";

  private final String LISTSET = "listset";
  private final String LISTFORMAT = "listformat";

  public OaiPmhIpml() {
    this.httpConnector(this);
  }

  public boolean connect(String serverAddresse, String from, String until, String metadataPrefix,
      String listCollection, String set) {
    boolean connected = false;
    do {
      try {

        connected = this
            .getComunication(serverAddresse, from, until, metadataPrefix, listCollection, set);

        if (connected == false) {
          return false;
        }

      } catch (Exception e) {

        LOG.error("Server-Kommunikation ist fehlgeschlagen!:  [" + e.getMessage() + "]");

        System.exit(4);
      }
    } while (!StringUtils.isEmpty(getResumptionToken()) && (getResumptionToken() != null));

    setVisible(false);

    return true;

  }

  public String getListRecords(String serverAddresse, String from, String until,
      String metadataPrefix, String set) {

    if (StringUtils.equals(getListset(), LISTSET) || StringUtils
        .equals(getListformat(), LISTFORMAT)) {

      if (StringUtils.equals(getListset(), LISTSET)) {

        String listRecord = serverAddresse + LIST_SETS;
        return listRecord;
      }
      if (StringUtils.equals(getListformat(), LISTFORMAT)) {

        String listRecord = serverAddresse + LIST_FORMAT;
        return listRecord;
      }
    } else {

      String buildListRecord = serverAddresse + metadataPrefix;

      if (StringUtils.isNotEmpty(from) && StringUtils.isNotEmpty(until)) {

        buildListRecord += FROM + from + UNTIL + until;

      } else if (StringUtils.isNotEmpty(from)) {

        buildListRecord += FROM + from;

      } else if (StringUtils.isNotEmpty(until)) {

        buildListRecord += UNTIL + until;
      }

      if (StringUtils.isNotEmpty(set)) {
        buildListRecord += SET + set;
      }

      String listRecords = StringUtils
          .replace(buildListRecord.replaceAll("\\s+", ""), "&from=null", "")
          .replace("&until=null", "").replace("&set=null", "");

      return listRecords;
    }

    return null;

  }

  public String getListRecordCollection(String collection) {
    String listRecordCollection = collection + getResumptionToken();
    return listRecordCollection;
  }

}
