package de.tib.hannover.joai.client;

import de.tib.hannover.joai.service.HarvestService;
import de.tib.hannover.joai.service.impl.HarvestServiceImpl;
import de.tib.hannover.joai.util.JOAIUtils;
import de.tib.hannover.joai.util.OaiPmhConstants;
import de.tib.hannover.joai.validator.Validator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class JOAI extends Validator {

  private static String setValue = null;

  private static File makeSupplierDir = null;
  private static HarvestService service = null;
  private static String metadataPrefix = null;
  private static File makeSupplierLocation = null;
  private static GregorianCalendar gregorianCalender = null;
  private static Date currentTime = null;
  private static String workingDirectory = null;
  private static String configDirectoryLocation = null;
  private static SimpleDateFormat simpleDateFormat = null;
  private static String url = null;

  private static final String LISTSET = "listset";
  private static final String LISTFORMAT = "listformat";
  private static final String LISTIDENTIFIER = "listidentifier";
  private static final String CONFIGDIRECTORYLOCATION = "configDirectoryLocation";

  private static String DATE_FORMAT = "yyyy-MM-dd";
  private static String TIME = "Time = ";

  protected static String METADATA = "/metadata/";

  private static final String DEFAULT = "default";

  private static final String CONFIG_PATTERN = "/configPattern.properties";
  private static final String CONFIG_PARAMETER = "/configParameter.properties";
  private static final String COFIG_DIRECTORY = "/configDirectoryLocation.properties";

  private static Logger LOG = LoggerFactory.getLogger(JOAI.class);

  protected static void startApplication() {

    Properties configParameterProp = new Properties();

    Properties configDirProp = new Properties();

    Properties configPatrernProp = new Properties();

    setSimpleDateFormat(new SimpleDateFormat(DATE_FORMAT));
    setCurrentTime(new Date());
    setGregorianCalender(new GregorianCalendar());

    try {

      configDirProp
          .load(new FileInputStream(new File(configDirectories() + COFIG_DIRECTORY)));

      configPatrernProp
          .load(new FileInputStream(new File(configDirectories() + CONFIG_PATTERN)));

      configParameterProp.load(
          new FileInputStream(new File(configDirectories() + CONFIG_PARAMETER)));

    } catch (FileNotFoundException e1) {

      LOG.error(e1.getMessage());
      e1.printStackTrace();
      System.exit(1);

    } catch (IOException e1) {
      LOG.error(e1.getMessage());
      e1.printStackTrace();
      System.exit(0);
    }

    String metadataformat = null;
    String urlValue = null;
    String time = null;
    String workDirectory = null;
    String defaultPattern = null;

    try {

      metadataformat = StringUtils.split(configParameterProp.getProperty(getSupplierName()), ';')[1]
          .trim();

      urlValue = StringUtils.split(configParameterProp.getProperty(getSupplierName()), ';')[0]
          .trim();

      time = StringUtils.split(configParameterProp.getProperty(getSupplierName()), ';')[2].trim();

      workDirectory = configDirProp.getProperty(CONFIGDIRECTORYLOCATION);

      defaultPattern = configPatrernProp.getProperty(DEFAULT);

    } catch (Exception e) {

      String exeption = "";
      for (int i = 0; i < JOAIClient.getArguments().length; i++) {
        exeption = exeption + " " + JOAIClient.getArguments()[i];
      }

      System.out.println("Bitte prüfen Sie ihre Eingabeparameter!    " + "[" + exeption + " ]");
      System.exit(3);

    }

    setWorkingDirectory(workDirectory);

    String dir = StringUtils.deleteWhitespace(workDirectory) + getSupplierName() + METADATA;

    /**
     * Hier wird Haupt-Ordner angelegt z.B. aps
     */
    /// .../collection/datacite/metadata
    setRootSupplierLocation(new File(dir.trim()));
    getRootSupplierLocation().mkdirs();

    /**
     * Verzeichis bis metadata-Verzeichen erzeugt
     */
    String bulidSupplierDir = new JOAIClient().buildSupplierDir(dir);

    /**
     * hier wird unter Ordner angelegt mit form until aps_2017-01-31/
     */
    setMakeSupplierDir(new File(bulidSupplierDir)); 
    // ..../collection/datacite/metadata/datacite_2017-01-01/TIB.DAGST
    getMakeSupplierDir().mkdir();

    if (getListset() != null) {
      setFileName(LISTSET);
    } else if (getListformat() != null) {
      setFileName(LISTFORMAT);
    } else if (getListidentifier() != null) {
      setFileName(LISTIDENTIFIER);
    } else {
      setMetadateFileName();
    }

    showConsole();

    setService(new HarvestServiceImpl());

    try {

      /**
       * wenn über die Konsole das Parameter (-pattern=active oder
       * -p=active) aktiviert wird! <resumptionToken *.*>(.*)
       *
       */
      if (isTokenPattern()) {

        try {
          Properties Pattern_Properties = new Properties();

          Pattern_Properties
              .load(new FileInputStream(new File(configDirectories() + CONFIG_PATTERN)));

          if (!StringUtils.equals(Pattern_Properties.getProperty(getSupplierName()), null)) {

            JOAIUtils.setConfigPattern(Pattern_Properties.getProperty(getSupplierName()));

          } else {
            System.out.println(
                "Verlag-Namenskürzel ist nicht konfiguriert! siehe Properties-Datei (configPattern.properties)");
            LOG.error("Verlag-Namenskürzel ist nicht konfiguriert!");
            System.exit(2);
          }

        } catch (FileNotFoundException e1) {

          LOG.error(e1.getMessage());
          e1.printStackTrace();
          System.exit(1);
        } catch (IOException e1) {

          LOG.error(e1.getMessage());
          e1.printStackTrace();
          System.exit(1);
        }

      } else {

        JOAIUtils.setConfigPattern(defaultPattern);

      }

      /**
       * configuration von ListRecordCollection
       */
      if (getListidentifier() != null) {

        OaiPmhConstants
            .setListRecordCollection(urlValue + OaiPmhConstants.getListIdentifierCollection());

      } else if (getListset() != null) {

        OaiPmhConstants.setListRecordCollection(urlValue + OaiPmhConstants.getListSetCollection());

      } else if (getListidentifier() == null && getListformat() == null) {

        OaiPmhConstants
            .setListRecordCollection(urlValue + OaiPmhConstants.getListRecordCollection());

      }

      int timeInterval = Integer.parseInt(time);

      if (timeInterval != 0) {
        showTimeInterval(time);
      }

      JOAIUtils.setTime(Long.parseLong(time));

      if (getListset() != null || getListformat() != null) {
        setUrl(urlValue);
      } else if (getListidentifier() != null) {
        setUrl(urlValue + OaiPmhConstants.getListIdentifiers());
      } else {
        setUrl(urlValue + OaiPmhConstants.getListrecords());
      }

      setMetadataPrefix(metadataformat);

      JOAIUtils.configLocation(bulidSupplierDir);

      configParameter();

      System.out.println(
          "Downloadzeit: [" + JOAIUtils.time(getGregorianCalender().getTimeInMillis()) + "]");

    } catch (Exception e) {

      LOG.error(e.getMessage());
      e.printStackTrace();
      JOAIUtils.deleteDir(getRootSupplierLocation());
    }

  }

  private static void showConsole() {
    System.out.println(JOAIClient.LINE + "\nDie Verbindung ist am  " + JOAIClient.getCurrentTime()
        + " gestartet ...." + "\n\n" + "Aktuelles-Verzeichnis:	" + getCurrentDirectory() + " \n");
  }

  private static void showTimeInterval(String time) {
    System.out.println(TIME + time + "\n");
  }

  private static void setFileName(String name) {
    OaiPmhConstants
        .setMetadatFileName(name + "_" + getSimpleDateFormat().format(getCurrentTime()) + "_T");
  }

  private static void setMetadateFileName() {
    OaiPmhConstants
        .setMetadatFileName(
            getSupplierName() + "_" + getSimpleDateFormat().format(getCurrentTime()) + "_T");
  }

  private static void configParameter() {

    if (getListset() != null) {
      getService().harvesting(getUrl(), null, null, null, null);

    } else if (getListformat() != null) {

      getService().harvesting(getUrl(), getFrom(), getUntil(), "", null);

    } else {
      getService()
          .harvesting(getUrl(), getFrom(), getUntil(), getMetadataPrefix(), getValueFromTerminal());
    }

  }

  protected static String getValueFromTerminal() {
    return setValue;
  }

  protected static void setValuesFromTerminal(String setValue1) {
    setValue = setValue1;
  }

  public static File getMakeSupplierDir() {
    return JOAI.makeSupplierDir;
  }

  public static void setMakeSupplierDir(File makeSupplierDir) {
    JOAI.makeSupplierDir = makeSupplierDir;
  }

  private static void setService(HarvestService service) {
    JOAI.service = service;
  }

  private static HarvestService getService() {
    return service;
  }

  private static void setMetadataPrefix(String metadataPrefix) {
    JOAI.metadataPrefix = metadataPrefix;
  }

  private static String getMetadataPrefix() {
    return metadataPrefix;
  }

  private static File getRootSupplierLocation() {
    return makeSupplierLocation;
  }

  private static void setRootSupplierLocation(File makeRootSupplierLocation) {
    JOAI.makeSupplierLocation = makeRootSupplierLocation;
  }

  private static void setUrl(String url) {
    JOAI.url = url;
  }

  private static String getUrl() {
    return url;
  }

  public static String getConfigDirectoryLocation() {
    return configDirectoryLocation;
  }

  protected void setConfigDirectoryLocation(String configDirectoryLocation1) {
    configDirectoryLocation = configDirectoryLocation1;
  }

  protected static Date getCurrentTime() {
    return currentTime;
  }

  protected static void setCurrentTime(Date currentTime) {
    JOAI.currentTime = currentTime;
  }

  protected static SimpleDateFormat getSimpleDateFormat() {
    return simpleDateFormat;
  }

  protected static void setSimpleDateFormat(SimpleDateFormat simpleDateFormat) {
    JOAI.simpleDateFormat = simpleDateFormat;
  }

  protected static GregorianCalendar getGregorianCalender() {
    return gregorianCalender;
  }

  protected static void setGregorianCalender(GregorianCalendar gregorianCalender) {
    JOAI.gregorianCalender = gregorianCalender;
  }

  public static String getWorkingDirectory() {
    return workingDirectory;
  }

  protected static void setWorkingDirectory(String workingDirectory) {
    JOAI.workingDirectory = workingDirectory;
  }

  protected static boolean validate(String s) {
    Pattern datePatt = Pattern
        .compile("([0-9]{4})-([0-9]{2})-([0-9]{2})([T][\\d]{2}\\:[\\d]{2}\\:[\\d]{2}[Z])?");

    Matcher m = datePatt.matcher(s);
    if (m.matches()) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Hier wird den sets- Parameter normaliziert, d.h wenn im String irgend
   * welche Sonderzeichen wie (|<>:"/\?*) beinhaltet ist, wird mit der Zeichen
   * "." umgeschrieben.
   *
   * @param string
   * @return
   */
  protected static String normalizeInputValues(String string) {

    if (!StringUtils.isEmpty(string)) {

      Pattern p = Pattern.compile("([/:?*\"|<>\\\\])");

      Matcher m = p.matcher(string);

      String normalizeString = string;
      while (m.find()) {
        normalizeString = StringUtils.replace(normalizeString, m.group(0), ".");
      }

      return normalizeString;
    }
    return null;
  }

}
