/**
 *
 */

package de.tib.hannover.joai.exeption;

/**
 * @author karimh
 */
public class OaiExeption extends Exception {

  private static final long serialVersionUID = 1L;

  public OaiExeption(String msg) {
    super(msg);
  }

  public OaiExeption(Exception e) {
    super(e);
  }

  public OaiExeption(String msg, Exception e) {
    super(msg, e);
  }
}
