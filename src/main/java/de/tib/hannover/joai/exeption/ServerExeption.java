package de.tib.hannover.joai.exeption;

/**
 * @author karimh
 */
public abstract class ServerExeption {

  public ServerExeption() {
  }

  public static final String SERVICE_UNAVAILABLE = " Der Server steht temporär nicht zur Verfügung, zum Beispiel wegen Ueberlastung oder Wartungsarbeiten. Bitte Time auf (20000) setzten!";

  public static final String MOVED_TEMPORARILY = "Die entsprechende URL steht vorübergehend nicht zur Verfügung, stattdessen steht der Inhalt temporär auf einer anderen URL (Location) bereit. Die ursprüngliche URL bleibt gültig.";

  public static final String MOVED_PERMANENTLY = "Die entsprechende URL wird dauerhaft auf eine andere URL (Location) weitergeleitet.";

  public static final String GONE = "Die entsprechende URL wurde dauerhaft entfernt.";

  public static final String TOO_MANY_REQUESTS = "Der Client hat zu viele Anfragen in einem bestimmten Zeitraum gesendet.";

  public static final String FORBIDDEN = "] Die Anfrage wurde mangels Berechtigung des Clients nicht durchgefuehrt.";

  public static final String INTERNAL_SERVER_ERROR = "] Der Server kann die angeforderten Daten nicht senden, weil auf dem Server ein Fehler aufgetreten ist.";

  public static final String NOT_IMPLEMENTED = "The Post method is not implemented by this URI";

  public static final String SERVER_EXPTION = "The server has not found anything matching the Request-URI: ";

  public static final String NOT_FOUND = "Die angeforderte Ressource wurde nicht gefunden";

  public static final String GATEWAY_TIMEOUT = "Gateway Zeitüberschreitung";
}
