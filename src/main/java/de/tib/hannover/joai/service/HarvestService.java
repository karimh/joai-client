package de.tib.hannover.joai.service;

import de.tib.hannover.joai.util.SupplierData;

public interface HarvestService {

  public void harvesting(String url, String string, String string2, String metadataPrefix,
      String set);

  public boolean harvesting(String url, String from, String until, String metadataPrefix,
      String recordCollection,
      String set);

  public void supplierData(SupplierData string);

}
