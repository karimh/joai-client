package de.tib.hannover.joai.service.impl;

import de.tib.hannover.joai.entity.impl.Records;
import de.tib.hannover.joai.service.HarvestService;
import de.tib.hannover.joai.util.SupplierData;

public class HarvestServiceImpl implements HarvestService {

  private SupplierData supplierData = null;
  private Records records;

  private SupplierData getSupplierDaten() {
    return supplierData;
  }

  protected void setSupplierDaten(SupplierData string) {
    this.supplierData = string;
  }

  public HarvestServiceImpl() {
    this.supplierData = new SupplierData();
  }

  public void harvesting(String url, String from, String until, String metadataPrefix, String set) {

    records = new Records();

    getSupplierDaten().setUrl(url);
    getSupplierDaten().setFrom(from);
    getSupplierDaten().setUntil(until);
    getSupplierDaten().setMetadataPrefix(metadataPrefix);
    getSupplierDaten().setSet(set);

    records.supplierDetails(getSupplierDaten());

  }

  public boolean harvesting(String url, String from, String until, String metadataPrefix,
      String recordCollection,
      String set) {
    return false;
  }

  public void supplierData(SupplierData supplierData) {
    // TODO Auto-generated method stub

  }

}
