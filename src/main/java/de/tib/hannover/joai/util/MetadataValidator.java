package de.tib.hannover.joai.util;

import de.tib.hannover.joai.exeption.JoaiExeption;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MetadataValidator {

  public static Logger LOG = LoggerFactory.getLogger(MetadataValidator.class);
  public static final String LOG_PATTERN = "[{}] [{}] [{}] {}";

  private static final String ERROR = "<error";
  private static final String RESUMPTIONTOKEN = "resumptionToken=";
  private static final String NO_RECORD_MACH = "noRecordsMatch";
  private static final String LIST_RECORD = "<ListRecords";
  private static final String LIST_IDENTIFIER = "<ListIdentifiers";
  private static final String LIST_FORMATS = "<ListMetadataFormats";
  private static final String LIST_SETS = "<ListSets";
  private static final String ENDLISTRECORDS = "<ListRecords/>";
  private static final String RECORD = "<record>";

  public static boolean flage = false;

  public static boolean isFlage() {
    return flage;
  }

  public static void setFlage(boolean flage) {
    MetadataValidator.flage = flage;
  }

  private static String token = null;

  private static String message = "";

  public static String getResumptionTokenValue(int index, String des, String pattern) {

    File sourceFile = new File(des);

    String content;

    try {
      content = FileUtils.readFileToString(sourceFile);

      Matcher matcher = Pattern.compile(pattern, Pattern.DOTALL).matcher(content);

      if (matcher.find()) {
        token = matcher.group(1);
      }

    } catch (IOException e) {

      e.printStackTrace();
    }

    return urlEncoder(StringUtils.replace(token, "\n", ""));
  }

  public static boolean getError(String des, String supplierName, String set) {

    File sourceFile = new File(des);
    String content;
    try {
      content = FileUtils.readFileToString(sourceFile);

      String p1 = ".*(<error *.*>(.*)|";
      String p2 = "<ListIdentifiers>(.*)</ListIdentifiers>|";
      String p3 = "<ListRecords *.*>(.*)<record>(.*)|";
      String p4 = "<ListMetadataFormats *.*>(.*)<metadataFormat>|";
      String p5 = "<ListSets *.*>(.*)<set>).*";

      Matcher matcher = Pattern.compile(p1 + p2 + p3 + p4 + p5, Pattern.DOTALL).matcher(content);

      if (matcher.find()) {

        String find = matcher.group(1);

        if (StringUtils.contains(find, ERROR)) {

          // Speziale Behandlung, wenn error Datei mit
          // resumptionToken-Element gliefert wird! s.
          // error-Verzeichnis ntrs
          if (StringUtils.contains(content, ERROR) && StringUtils
              .contains(content, RESUMPTIONTOKEN)) {
            setFlage(true);
            return true;
          }
          if (StringUtils.contains(find, NO_RECORD_MACH)) {
            LOG.info(LOG_PATTERN, supplierName, set, NO_RECORD_MACH, matcher.group(1));
          } else {
            LOG.error(LOG_PATTERN, supplierName, set, "ERROR", matcher.group(1));
          }
          setMessage(matcher.group(1));

          return false;
        } else if ((StringUtils.isNotEmpty(content)
            && ((StringUtils.contains(find, LIST_RECORD) && StringUtils.contains(find, RECORD))
            || StringUtils.contains(find, LIST_IDENTIFIER)
            || StringUtils.contains(find, LIST_FORMATS)
            || StringUtils.contains(find, LIST_SETS)))) {

          return true;
        }
      } else {
        // Speziale Behandlung, wenn die letzte Datei kein leere
        // resumptionToken-Element beinhaltet! s. error-Verzeichnis (base)
        if (StringUtils.contains(content, ENDLISTRECORDS) && StringUtils.isNotEmpty(content)) {
          setFlage(true);
          return true;
        } else {

          LOG.error(LOG_PATTERN, supplierName, set, "INVALID_RESPONSE",
              JoaiExeption.CONTENTINVALID + ": " + content.trim());
          setMessage(JoaiExeption.CONTENTINVALID);
          return false;
        }

      }

    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return false;
  }

  /**
   * OAI-PMH Before including a resumptionToken in the URL of a subsequent
   * request, a harvester must encode any special characters in it.
   *
   * @param code
   * @return
   */
  public static String urlEncoder(String url) {

    String[] d = {"%"};
    String[] e = {"%25"};

    String temp = StringUtils.replaceEach(url, d, e);

    String[] a = {"/", "?", "#", "=", "&", ":", ";", " ", "+", "|"};
    String[] b = {"%2F", "%3F", "%23", "%3D", "%26", "%3A", "%3B", "%20", "%2B", "%7C"};

    String urlEncoded = StringUtils.replaceEachRepeatedly(temp, a, b);

    return urlEncoded;
  }

  public static String getMessage() {
    return message;
  }

  public static void setMessage(String message) {
    MetadataValidator.message = message;
  }

}