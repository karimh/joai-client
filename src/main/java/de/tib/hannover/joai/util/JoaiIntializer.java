package de.tib.hannover.joai.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

public abstract class JoaiIntializer {

  private final static String VERSION = "Implementation-Version";
  private final static String REVISION = "VCS-Revision";
  private final static String MANIFEST = "/META-INF/MANIFEST.MF";

  public static String getApplicationVersionEntries() {

    return determineApplicationManifestEntries(VERSION);
  }

  public static String getApplicationRevisionEntries() {
    return determineApplicationManifestEntries(REVISION);
  }

  private static String determineApplicationManifestEntries(String manifestAttribut) {

    Class<JoaiIntializer> clazz = JoaiIntializer.class;
    String className = clazz.getSimpleName() + ".class";
    String classPath = clazz.getResource(className).toString();

    String manifestPath = classPath.substring(0, classPath.lastIndexOf("!") + 1) + MANIFEST;

    Manifest manifest;
    String value = null;

    try {
      manifest = new Manifest(new URL(manifestPath).openStream());

      Attributes attr = manifest.getMainAttributes();
      value = attr.getValue(manifestAttribut);

    } catch (MalformedURLException e) {

    } catch (IOException e) {

    }
    return value;

  }

}
