package de.tib.hannover.joai.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DeleteEmptyFolder {

  private final List<File> emptyFolders;

  public DeleteEmptyFolder(String path) {
    this(new File(path));
  }

  public DeleteEmptyFolder(File dir) {
    emptyFolders = new ArrayList<File>();
    listEmptyFolders(dir);
  }

  public void deleteEmptyFolder() {
    for (File emptyDir : emptyFolders) {
      deleteFolder(emptyDir);
    }
  }

  private boolean deleteFolder(File dir) {
    if (dir.isDirectory()) {
      for (File file : dir.listFiles()) {
        deleteFolder(file);
      }
      return dir.delete();
    }
    return false;
  }

  private void listEmptyFolders(File dir) {
    if (isEmpty(dir)) {
      emptyFolders.add(dir);
    } else {
      for (File file : dir.listFiles()) {
        if (file.isDirectory()) {
          listEmptyFolders(file);
        }
      }
    }
  }

  private boolean isEmpty(File dir) {
    if (dir.isDirectory()) {
      for (File file : dir.listFiles()) {
        if (file.isDirectory()) {
          if (!isEmpty(file)) {
            return false;
          }
        } else {
          return false;
        }
      }
      return true;
    }
    return false;
  }

}